<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\facades\DB;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\model\Aplikasi;

class BackendController extends BaseController{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function Index(){
		return view('backend.dashboard');	
	}

	public function Aplikasi(Request $request){

		$this->validate($request, [
			'limit' => 'integer',
		]);

		$keyword 			= $request->get('keyword');
		$kategori_aplikasi  = $request->get('kategori_aplikasi');
		$tahun_lulus		= $request->get('tahun_lulus');
		
		$data = Aplikasi::where('nama_aplikasi', 'LIKE', '%' . $keyword . '%')
						  ->where('kategori_aplikasi', 'LIKE', '%' . $kategori_aplikasi. '%')
						  ->where('tahun_lulus', 'LIKE', '%' . $tahun_lulus. '%')
						  ->paginate($request->limit ? $request->limit : 5);

		$data->appends($request->only('keyword', 'kategori_aplikasi', 'tahun_lulus', 'limit'));

		return view('backend.aplikasi', compact('data', 'keyword', 'kategori_aplikasi', 'tahun_lulus'));
	}

	public function Tambah_aplikasi(){
		return view('backend.tambah_aplikasi');
	}

	public function store(Request $request){
		$data 						= new Aplikasi();
		$data->nama_aplikasi 		= $request->input('nama_aplikasi');
		$data->kategori_aplikasi 	= $request->input('kategori_aplikasi');;
		$data->nim 					= $request->input('nim');
		$data->nama_pembuat 		= $request->input('nama_pembuat');
		$data->tahun_lulus 			= $request->input('tahun_lulus');
		$data->deskripsi_aplikasi 	= $request->input('deskripsi_aplikasi');
		$data->alamat_tautan 		= $request->input('alamat_tautan');

		$file = $request->file('gambar_aplikasi');
		$ext = $file->getClientOriginalExtension();
		$newName = rand(100000,1001238912).".".$ext;
		$file->move('uploads/gambar_aplikasi',$newName);
		$data->gambar_aplikasi = $newName;

		$data->save();
		return redirect('/admin/aplikasi')->with('alert-success','Data berhasil ditambahkan!');
	}

	public function Kategori(){
		return view('backend.kategori');
	}

}
