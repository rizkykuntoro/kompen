<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Aplikasi extends Model
{
	protected $table = 'aplikasi'; 

	protected $fillable = [
		'id_aplikasi',
		'nama_aplikasi',
		'kategori_aplikasi',
		'nim',
		'nama_pembuat',
		'tahun_lulus',
		'deskripsi_aplikasi',
		'alamat_tautan',
		'gambar_aplikasi',
		'updated_at',
		'created_at'
	];
}
