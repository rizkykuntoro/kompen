@extends('backend.layout.index')
@section('content')
<!-- Dashboard -->

<!-- Content -->
<div class="dashboard-content">

	<!-- Titlebar -->
	<div id="titlebar">
		<div class="row">
			<div class="col-md-12">
				<h2>Howdy, Tom!</h2>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>Aplikasi</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>

	<!-- Notice -->
	@if(Session::has('alert-success'))
	<div class="row">
		<div class="col-md-12">
			<div class="notification success closeable margin-bottom-30">
				<div class="alert alert-success">
					<strong>{{ \Illuminate\Support\Facades\Session::get('alert-success') }}</strong>
				</div>
				<a class="close" href="#"></a>
			</div>
		</div>
	</div>
	@endif

	<!-- Content -->

	<div class="row">
		<form action="{{ url()->current() }}" method="GET" autocomplete="off">
			<div class="col-md-12">
				<div class="col-md-5">
					<input type="text" name="keyword" placeholder="Masukan Kata Kunci">
				</div>
				<div class="col-md-3">
					<select name="kategori_aplikasi">
						<option value="">Pilih Kategori</option>
						<option value="mobile">Mobile</option>
						<option value="website">Website</option>
						<option value="lainnya">Lainnya</option>
					</select>
				</div>
				<div class="col-md-2">
					<input type="text" placeholder="Tahun" name="tahun_lulus">
				</div>
				<div class="col-md-2">
					<button class="button" type="submit">Search</button>
				</div>
			</div>
		</form>
	</div>

	<div class="col-lg-12 col-md-12">
		<div class="dashboard-list-box margin-top-0">
			<h4>Daftar Aplikasi <a class="button pull-right" href="{{ url('admin/aplikasi/tambah') }}" style="background-color: #2ecc71">Tambah data</a></h4>

			@if (count($data) > 0)
			<ul>
				@foreach ($data as $key =>$isi)
				<li>
					<div class="list-box-listing">
						<div class="list-box-listing-img"><a href="#"><img src="{{ url('uploads/gambar_aplikasi/'.$isi->gambar_aplikasi) }}" alt=""></a></div>
						<div class="list-box-listing-content">
							<div class="inner">
								<h3>{{ $isi->nama_aplikasi }}</h3>
								<span>{{ $isi->nama_pembuat }}</span>
								<div class="star-rating" data-rating="5.0">
									<div class="rating-counter">(31 reviews)</div>
								</div>
							</div>
						</div>
					</div>
					<div class="buttons-to-right">
						<a href="#" class="button gray"><i class="sl sl-icon-note"></i> Edit</a>
						<a href="#" class="button gray"><i class="sl sl-icon-close"></i> Delete</a>
					</div>
				</li>
				@endforeach
				@else

				<div>
					<br><br>
					<center>Tidak Ada Hasil</center>
					<br><br>
				</div>
				@endif
			</ul>
		</div>
		{{ $data->render() }}
	</div>
</div>

@stop