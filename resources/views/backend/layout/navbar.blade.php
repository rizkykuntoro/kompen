<div id="dashboard">

	<!-- Navigation-->

	<!-- Responsive Navigation Trigger -->
	<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

	<div class="dashboard-nav">
		<div class="dashboard-nav-inner">

			<ul data-submenu-title="Main">
				<li class="{{ Request::is('admin') ? 'active' : '' }}">
					<a href="{{ url('admin/')}}"><i class="sl sl-icon-settings"></i> Dashboard</a>
				</li>
			</ul>

			<ul data-submenu-title="Listings">
				<li class="{{ Request::is('admin/aplikasi' || 'admin/kategori') ? 'active' : '' }}">
					<a><i class="sl sl-icon-layers"></i> Data</a>
					<ul>
						<li class="{{ Request::is('admin/aplikasi' || 'admin/aplikasi/tambah') ? 'active' : '' }}" >
							<a href="{{ url('admin/aplikasi') }}"> Aplikasi</a>
						</li>
						<li class="{{ Request::is('admin/kategori') ? 'active' : '' }}" >
							<a href="{{ url('admin/kategori') }}"> Kategori</a>
						</li>
					</ul>	
				</li>
			</ul>	

			<ul data-submenu-title="Account">
				<li><a href="dashboard-my-profile.html"><i class="sl sl-icon-user"></i> Profil Saya</a></li>
				<li><a href="index.html"><i class="sl sl-icon-power"></i> Logout</a></li>
			</ul>

		</div>
	</div>
	<!-- Navigation / End -->