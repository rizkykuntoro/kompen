<!DOCTYPE html>
<head>

	<!-- Basic Page Needs-->
	<title>Tugas Akhir</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS -->
	<link rel="stylesheet" href="{!! asset('template/css/style.css') !!}">
	<link rel="stylesheet" href="{!! asset('template/css/colors/main.css') !!}" id="colors">

</head>

<body>

	@include('backend.layout.header')

	@include('backend.layout.navbar')

	@yield('content')

	@include('backend.layout.footer')

</body>
</html>