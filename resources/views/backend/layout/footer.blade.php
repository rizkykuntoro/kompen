<!-- Copyrights -->
<div class="col-md-12">
	<div class="copyrights">© 2017 Listeo. All Rights Reserved.</div>
</div>
</div>

</div>
<!-- Content / End -->


</div>
<!-- Dashboard / End -->


</div>
<!-- Wrapper / End -->


<!-- Scripts -->
<script type="text/javascript" src="{!! asset('template/scripts/jquery-2.2.0.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/mmenu.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/chosen.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/slick.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/rangeslider.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/magnific-popup.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/waypoints.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/counterup.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/jquery-ui.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/tooltips.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/custom.js') !!}"></script>

<!-- Maps -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script type="text/javascript" src="{!! asset('template/scripts/infobox.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/markerclusterer.js') !!}"></script>
<script type="text/javascript" src="{!! asset('template/scripts/maps.js') !!}"></script>

<!-- Booking Widget - Quantity Buttons -->
<script src="{!! asset('template/scripts/quantityButtons.js') !!}"></script>

<!-- Date Range Picker - docs: http://www.daterangepicker.com/ -->
<script src="{!! asset('template/scripts/moment.min.js') !!}"></script>
<script src="{!! asset('template/scripts/daterangepicker.js') !!}"></script>






