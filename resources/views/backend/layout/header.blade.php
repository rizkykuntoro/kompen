<!-- Wrapper -->
<div id="wrapper">

<!-- Header Container
	================================================== -->
	<header id="header-container" class="fixed fullwidth dashboard">

		<!-- Header -->
		<div id="header" class="not-sticky">
			<div class="container">

				<!-- Left Side Content -->
				<div class="left-side">

					<!-- Logo -->
					<div id="logo">
						<a href="index.html"><img src="images/logo.png" alt=""></a>
						<a href="{{ url('/') }}" class="dashboard-logo"><img src="{!! asset('template/images/logo2.png') !!}" alt=""></a>
					</div>

				</div>
				<!-- Left Side Content / End -->

				<!-- Right Side Content / End -->
				<div class="right-side">
					<!-- Header Widget -->
					<div class="header-widget">

						<!-- User Menu -->
						<div class="user-menu">
							<div class="user-name"><span><img src="images/dashboard-avatar.jpg" alt=""></span>My Account</div>
							<ul>
								<li><a href="dashboard.html"><i class="sl sl-icon-settings"></i> Dashboard</a></li>
								<li><a href="dashboard-messages.html"><i class="sl sl-icon-user"></i> Profil Saya</a></li>
								<li><a href="index.html"><i class="sl sl-icon-power"></i> Logout</a></li>
							</ul>
						</div>


					</div>
					<!-- Header Widget / End -->
				</div>
				<!-- Right Side Content / End -->

			</div>
		</div>
		<!-- Header / End -->

	</header>
	<div class="clearfix"></div>
<!-- Header Container / End -->