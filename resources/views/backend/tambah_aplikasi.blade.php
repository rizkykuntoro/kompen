@extends('backend.layout.index')
@section('content')
<!-- Dashboard -->

<!-- Content -->
<div class="dashboard-content">

	<!-- Titlebar -->
	<div id="titlebar">
		<div class="row">
			<div class="col-md-12">
				<h2>Howdy, Tom!</h2>
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>Tambah Aplikasi</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>

	<!-- Notice -->
	<!-- <div class="row">
		<div class="col-md-12">
			<div class="notification success closeable margin-bottom-30">
				<p>Your listing <strong>Hotel Govendor</strong> has been approved!</p>
				<a class="close" href="#"></a>
			</div>
		</div>
	</div> -->

	<!-- Content -->

	<div class="col-lg-12 col-md-12">
		<div class="dashboard-list-box margin-top-0">
			<h4 class="gray">Tambah Aplikasi</h4>
			<div class="dashboard-list-box-static">

				<form action="{{ route('BackendController.store') }}" method="post" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<!-- Avatar -->
					<div class="edit-profile-photo">
						<img src="{!! asset('template/images/user-avatar.jpg') !!}" alt="">
						<div class="change-photo-btn">
							<div class="photoUpload">
								<span><i class="fa fa-upload"></i> Unggah foto</span>
								<input type="file" class="upload" name="gambar_aplikasi">
							</div>
						</div>
					</div>

					<!-- Details -->
					<div class="my-profile">

						<label>Nama Aplikasi</label>
						<input type="text" placeholder="Masukan Nama Aplikasi" name="nama_aplikasi">

						<label>Kategori Aplikasi</label>
						<select name="kategori_aplikasi">
							<option value="mobile">Mobile</option>
							<option value="website">Website</option>
							<option value="lainnya">Lainnya</option>
						</select>

						<label>NIM</label>
						<input type="text" placeholder="Masukan NIM" name="nim">

						<label>Nama Pembuat</label>
						<input type="text" placeholder="Masukan Nama Pembuat" name="nama_pembuat">

						<label>Tahun Lulus</label>
						<input type="text" placeholder="Masukan Tahun Lulus" name="tahun_lulus">

						<label>Deskripsi Aplikasi</label>
						<textarea id="notes" cols="30" rows="10" name="deskripsi_aplikasi">Tuliskan Deskripsi Aplikasi Disini..</textarea>

						<label><i class="fa fa-globe"></i> Alamat Tautan</label>
						<input style="margin-bottom: 0px" placeholder="https://www.google.com/" type="text" name="alamat_tautan">
						<small >*Sertakan "http://" pada awal penulisan tautan</small>
					</div>
					<input type="submit" class=" pull-right button margin-top-15" value="Simpan">
					<div class="clearfix"></div>
				</form>

			</div>
		</div>
	</div>
</div>

@stop