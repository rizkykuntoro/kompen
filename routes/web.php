<?php

/* 		Frontend 	*/
//////////////////////
Route::get('/', function () { return view('welcome'); });


 /*		Backend 	*/
 /////////////////////
 
 /*dashboard*/
Route::get('/admin','BackendController@Index');

/* Aplikasi */
Route::resource('BackendController','BackendController');
Route::get('/admin/aplikasi','BackendController@Aplikasi');

Route::get('/admin/aplikasi/tambah','BackendController@Tambah_aplikasi');

/* Kategori */
Route::get('/admin/kategori','BackendController@Kategori');
